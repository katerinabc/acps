#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 25 23:49:56 2017

@author: katerinadoyle
"""


#Set Data Directory
import os
os.chdir("/Users/katerinadoyle/Documents/gitrepo/acps")
cwd = os.getcwd()

##############################
# MODULES
##############################

# general modules
import itertools # for flatting list of lists
import pickle #to save dtm
import csv # to create output of pooled tweets 

# working with strings
import re #for regular exptression
import string

# math modules
import pandas as pd
import numpy as np

#for drawing
import matplotlib.pyplot as plt

# for txt analysis
import gensim
from gensim import corpora, models, similarities
from gensim.models.wrappers.dtmmodel import DtmModel
from sklearn.manifold import MDS
from sklearn.feature_extraction.text import CountVectorizer
import nltk #for text analytics
from nltk.tokenize import word_tokenize
# load nltk's English stopwords as variable called 'stopwords'
stoplist = nltk.corpus.stopwords.words('english')
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string

# to visualize results of text analysis
import pyLDAvis
import pyLDAvis.gensim

############################################################

#  PREPROCESSING FOR STATIC TOPIC MODELING

# NOTE: copied everything from dynamic lda tutorial except the creation of a sequential file
############################################################

# creates a file 'meta data' w/ info: tag + cleaned tweet(s)

dat_outfile = open(os.path.join(cwd, 'analysis_static_tags', 'metadata.dat'), 'w')

# CHANGE HEADER IF NECESSARY
dat_outfile.write('tag\tcontent\n') #write header              
                 
tweets = list()
tweets_user = list()
#Set total_tweets list per year, starting at 0
total_tweets_list = [0 for tag in htag]

#Analyze each year..

tag_count = 0

for tag in htag: #For each year

    print('Analyzing tag ' + str(tag))
    
    #Set total_tweets to 0
    total_tweets = 0
    
    #For each result (tweet), get content and save it to the output file if it's not an empty line
    for row in twp_pool.itertuples():
        
        text = row[1]
        #line = line.tolist()
        
        #utf-8 encoding
#        try:
#            text = line.decode('utf-8')
#        except:
#            pass

        #Remove @xxxx and #xxxxx
        #content = preprocess(line) #still includes url, @ and re. Only removes emicons
        #content = [x[0] for x in content]
        content = [word.lower() for word in text.split() if word.find('@') == -1 and word.find('#') == -1 and word.find('http') == -1]
        #content = [word.lower() for word in line[1].split() if word.find('@') == -1 and word.find('#') == -1 and word.find('http') == -1]
        
        
        #join words list to one string
        content = ' '.join(content)
        
        
        #remove symbols
        content = re.sub(r'[^\w]', ' ', content)
        
        #remove stop words
        content = [word for word in content.split() if word not in stopwords.words('english') and len(word) > 3 and not any(c.isdigit() for c in word)]
        
        #join words list to one string
        content = ' '.join(content)

        #Stemming and lemmatization
        lmtzr = WordNetLemmatizer()
        
        content = lmtzr.lemmatize(content)
        
        #Filter only nouns and adjectives
        tokenized = nltk.word_tokenize(content)
        classified = nltk.pos_tag(tokenized)

        content = [word for (word, clas) in classified if clas == 'NN' or clas == 'NNS' or clas == 'NNP' or clas == 'NNPS' or clas == 'JJ' or clas == 'JJR' or clas == 'JJS']
        #join words list to one string
        content = ' '.join(content)
        
        
        if len(content) > 0:
            tweets.append([content, row[]]) # needs to be content, tag
            tweets_user.append([content, row[], row[2]]) #content tag, user
            total_tweets += 1
            #right encoding
            content = content.encode('ascii', 'ignore').decode('ascii')
            dat_outfile.write(str(row[2]) + '\t' + content)
            dat_outfile.write('\n')
            
    #Add the total tweets to the total tweets per year list
    total_tweets_list[tag_count] += total_tweets
            
    tag_count+=1

dat_outfile.close() #Close the tweets file+=1

dat_outfile.close() #Close the tweets file

# documents = line in metadata.dat

# read documents line by line and create vector
# document represented by 1 vector where ith element in vector is frequency w/ which ith word appears in document
# dictionary to map words to ids

dictionary = corpora.Dictionary(line[1].lower().split() for line in tweets)

# remove stop words and words that appear only once
stop_ids = [dictionary.token2id[stopword] for stopword in stoplist
            if stopword in dictionary.token2id]
once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.items() if docfreq == 1]
dictionary.filter_tokens(stop_ids + once_ids) # remove stop words and words that appear only once
dictionary.compactify() # remove gaps in id sequence after words that were removed

dictionary.save(os.path.join(cwd, 'analysis_static_tags','dictionary.dict')) # store the dictionary, for future reference

#Write seq file
seq_outfile = open(os.path.join(cwd, 'analysis_static_tags', 'acps-static-seq.dat'), 'w')
seq_outfile.write(str(len(total_tweets_list)) + '\n') #number of TimeStamps

for count in total_tweets_list:
    seq_outfile.write(str(count) + '\n') #write the total tweets per year (timestamp)
    
seq_outfile.close()

print('Done collecting tweets and writing seq')

#Save vocabulary
vocFile = open(os.path.join(cwd, 'analysis_static_tags', 'vocabulary.dat'),'w')
for word in dictionary.values():
    word = str(word.encode('utf-8'))
    vocFile.write(word+'\n')
    
vocFile.close()

#Prevent storing the words of each document in the RAM
class MyCorpus(object):
     def __iter__(self):
         for line in tweets:
             data = line[0]
             # assume there's one document per line, tokens separated by whitespace
             yield dictionary.doc2bow(data.lower().split())


corpus = MyCorpus()

print(corpus)

multFile = open(os.path.join(cwd, 'analysis_static_tags', 'acps-static--mult.dat'),'w')

for vector in corpus: # load one vector into memory at a time
    multFile.write(str(len(vector)) + ' ')
    for (wordID, weigth) in vector:
        multFile.write(str(wordID) + ':' + str(weigth) + ' ')

    multFile.write('\n')
    
multFile.close()

# serialize corpus for future usage
corpora.MmCorpus.serialize(os.path.join(cwd, 'analysis_static_tags', 'corpus_static.mm'), corpus)
