#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 25 23:53:07 2017

@author: katerinadoyle
"""
#Set Data Directory
import os
os.chdir("/Users/katerinadoyle/Documents/gitrepo/acps")
cwd = os.getcwd()

##############################
# MODULES
##############################

# general modules
import csv # to create output of pooled tweets 

# working with strings
import string

# for txt analysis
import gensim
from gensim import corpora, models
import nltk #for text analytics
# load nltk's English stopwords as variable called 'stopwords'
stoplist = nltk.corpus.stopwords.words('english')
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")


# loads the corpus
corpus = corpora.MmCorpus(os.path.join(cwd, 'analysis_static_tags', 'corpus_static.mm'))
print(corpus)           

dictionary = gensim.corpora.Dictionary.load(os.path.join(cwd, "analysis_static_tags","dictionary.dict"))
 

############################################################
# create network graph w/ topics & users
# code from https://github.com/jrladd/inaugural/blob/master/src/gensim_lda.py
# changes: addresses becomes users (variable name not changed)

#Return topic distribution for all documents (create a new combined corpus using the lda model)
m_lda5 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model5'))
m_lda7 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model7'))
m_lda9 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model9'))

lda = m_lda7
num_topics = 7

# Assigns the topics to the documents in corpus
corpus_lda = lda[corpus]
# creates for each document a list with probability of being in that topic

#Create two node lists for NetworkX
# mode 1 = topics
topics = {t[0]:t[1] for t in lda.print_topics(num_topics)}  #dict comprehension

# mode 2 = users
# first get tweets,then connect tweets to user using the twp dataset as lookup variable

addresses = list(tweets_user) #used here first twp_tags which resulted in out of index error when creating edges
# transform node 1 into 1 string. need to use list format for that. tuples can't be modified
addresses = [''.join(item[0]) for item in addresses]
# convert addressses to dict
addresses_dict = {t[1]:t[0] for t in addresses}

#Create edge list for NetworkX. Created the bigraph in R. Took 30 minutes. 
# Learn to switch from python to R when python skills not up to the task 
edges = [[(addresses[idx], t[0], t[1]) for t in y] for idx,y in enumerate(corpus_lda)]
edges = sum(edges, []) #has to be a tuple. now in a list for. 
edge_tpl = tuple(edges)

with open('edgelist.csv','w', encoding='utf-8') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(['mode1','mode2', 'weight'])
    for row in edges:
        csv_out.writerow(row)