#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 17:14:27 2017

@author: katerinadoyle
"""

#Set Data Directory
import os
os.chdir("/Users/katerinadoyle/Documents/gitrepo/acps")

##############################
# MODULES
##############################

# general modules
import itertools # for flatting list of lists
import pickle #to save dtm
import csv # to create output of pooled tweets 

# working with strings
import re #for regular exptression
import string

# math modules
import pandas as pd
import numpy as np

#for drawing
import matplotlib.pyplot as plt

# for txt analysis
import gensim
from gensim import corpora, models, similarities
from sklearn.manifold import MDS
from sklearn.feature_extraction.text import CountVectorizer
import nltk #for text analytics
from nltk.tokenize import word_tokenize
# load nltk's English stopwords as variable called 'stopwords'
stoplist = nltk.corpus.stopwords.words('english')
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string

# to visualize results of text analysis
import pyLDAvis
import pyLDAvis.gensim



##############################
# EXECUTIVE FUNCTIONS
##############################

#Tokenize the tweets

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r'(RT)', # Retweets to use
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r'(?:[\w_]+)', # other words
    r'(?:\S)' # anything else
]
    
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)
retweet_re = re.compile(r'(?<=RT\s@)([\w_]+)(?=[\s:,])', re.IGNORECASE|re.MULTILINE|re.UNICODE)			# Retweet regex statement
tweetat_re = re.compile(r'(?<!RT\s@)(?<=@)[\w_]+(?=[\s:,])', re.IGNORECASE|re.MULTILINE)	# Tweet-at regex statement

#this tokenizer recognizes RT and special twitter character as single tokens. 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s, lowercase=False):
    tokens = tokenize(s)
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
        #tokens = [token if retweet_re.search(token) else token.lower() for token in tokens]
    return tokens


def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems


def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens

def extract_hash_tags(s):
    tag = [part[1:] for part in s.split() if part.startswith('#')]
    return tag

def findhashtag(tag, tweet):
    findtag = "#"+tag
    match = re.search(findtag, tweet)
    if match:
        return True
    return False



##############################
# LOAD DATA
##############################

all_data = pd.DataFrame() #creates blank dataframe
df = pd.read_excel("acps1.xlsx",1) #name of excle file, index for sheet, 
x = list(df) #to check column headers
df= df.drop(df.columns[[-3]], axis=1) #axes = 1 for columns, axes = 0 for rows
all_data = all_data.append(df, ignore_index=True)

df = pd.read_excel("acps2.xlsx", 1)
all_data = all_data.append(df, ignore_index=True)
 #below for loop if headers don't need to be modified
 #for f in glob.glob("acps*.xlsx"): #checks for files with matching names
 #    df = pd.read_excel(f, 1) #reads the file using pandas
 #    all_data = all_data.append(df,ignore_index=True) #appends the file using append function (concat cut also be used)


#check file
all_data.describe()
all_data[:2] #look at first 2 rows
 #print (list(all_data)) #get column headings

#clean file: Delete empty rows, rows with NaN in column "from_user"
all_data = all_data.dropna(subset=['from_user'])

#fname = r"acps_data_tokenized.csv"
#data = pd.read_csv(fname, low_memory=False)
data = all_data
data.shape #(34388, 51)
#data.info
#data.text.describe
data.text.isnull().values.any() #check for missing value
data.columns
df = data.dropna(subset=['text'])#remove missing values
print(data.shape[0] - df.shape[0], "missing/empty text values") #(34312, 50) (34388, 50)


##############################
# CREATE SAMPLE FILE TO TEST SCRIPT
##############################

df['text'].describe

def count_letters(word):
    return len(word) - word.count(' ')

leng_tweets =[]
for text in df['text']:
    numb_char = count_letters(text)
    leng_tweets.append(numb_char)
    
leng_tweets = pd.DataFrame(leng_tweets)
char_mean = leng_tweets.mean() #105.89
char_median =leng_tweets.median() #115
df['lng_tweets'] = leng_tweets

df_small = df
print(df_small.shape)
#type(df_small['lng_tweets'])
df_small = df_small[df_small['lng_tweets'] > 115] 
df_small = df_small[0:500] # run this line only to create a tiny dataframe for testing the script
print(df_small.shape)

##############################
# GROUP TWEETS BY USERNAME. KEEP TIME VARIABLE INTACT
##############################


df_small.columns # get the column names

# get list of unique hashtags
    
hashtag = df_small['text'].apply(extract_hash_tags).values.tolist()
htag = list(itertools.chain.from_iterable(hashtag))
htag = list(set(htag)) # return unique hashtags
htag = [ re.sub('[^\w]','', tag) for tag in htag ] # remove special characters

# pool tweets per hashtag
             
tweet_pooled = [ ]
for tag in htag:
    tag_tweet = []
   # tag_name = tag
    for i in range(1,len(df_small)):
        if findhashtag(tag, df_small.iloc[i,2]):
            tag_tweet = [tag, df_small.iloc[i,1:18]]
            tweet_pooled.append(tag_tweet)
            
twp = pd.DataFrame.from_items(tweet_pooled).transpose()
twp['tags'] = twp.index.values
twp.index = range(0, len(twp))
twp.to_csv("tweet_pooled.csv", sep=';', mode = 'a', header = True, index=True)

# add tweets with same tag into one line. 
#twp_tags = twp.loc[:,'text'].groupby('tags').agg(lambda x:x.tolist()) # groups all column entries into lists for matching tags
twp_tags = twp.groupby('tags', sort=False)['text'].apply(' '.join).to_frame()
twp_user = twp.groupby('from_user')['text'].apply(' '.join).to_frame()

# turn index values into column 'tags'  and create numberical index
twp_tags['tags'] = twp_tags.index.values
twp_tags.index = range(0, len(twp_tags))

############################################################

#  PREPROCESSING FOR STATIC TOPIC MODELING

# NOTE: copied everything from dynamic lda tutorial except the creation of a sequential file
############################################################

# creates a file 'meta data' w/ info: tag + cleaned tweet(s)
cwd = os.getcwd()
dat_outfile = open(os.path.join(cwd, 'analysis_static_tags', 'metadata.dat'), 'w')

# CHANGE HEADER IF NECESSARY
dat_outfile.write('tag\tcontent\n') #write header              
                 
tweets = list()
#Set total_tweets list per year, starting at 0
total_tweets_list = [0 for tag in htag]

#Analyze each year..

tag_count = 0

for tag in htag: #For each year

    print('Analyzing tag ' + str(tag))
    
    #Set total_tweets to 0
    total_tweets = 0
    
    #For each result (tweet), get content and save it to the output file if it's not an empty line
    for row in twp_tags.itertuples():
        
        text = row[1]
        #line = line.tolist()
        
        #utf-8 encoding
#        try:
#            text = line.decode('utf-8')
#        except:
#            pass

        #Remove @xxxx and #xxxxx
        #content = preprocess(line) #still includes url, @ and re. Only removes emicons
        #content = [x[0] for x in content]
        content = [word.lower() for word in text.split() if word.find('@') == -1 and word.find('#') == -1 and word.find('http') == -1]
        #content = [word.lower() for word in line[1].split() if word.find('@') == -1 and word.find('#') == -1 and word.find('http') == -1]
        
        
        #join words list to one string
        content = ' '.join(content)
        
        #remove symbols
        content = re.sub(r'[^\w]', ' ', content)
        
        #remove stop words
        content = [word for word in content.split() if word not in stopwords.words('english') and len(word) > 3 and not any(c.isdigit() for c in word)]
        
        #join words list to one string
        content = ' '.join(content)

        #Stemming and lemmatization
        lmtzr = WordNetLemmatizer()
        
        content = lmtzr.lemmatize(content)
        
        #Filter only nouns and adjectives
        tokenized = nltk.word_tokenize(content)
        classified = nltk.pos_tag(tokenized)

        content = [word for (word, clas) in classified if clas == 'NN' or clas == 'NNS' or clas == 'NNP' or clas == 'NNPS' or clas == 'JJ' or clas == 'JJR' or clas == 'JJS']
        #join words list to one string
        content = ' '.join(content)
        
        
        if len(content) > 0:
            tweets.append([content, row[2]]) # needs to be content, tag
            total_tweets += 1
            #right encoding
            content = content.encode('ascii', 'ignore').decode('ascii')
            dat_outfile.write(str(row[2]) + '\t' + content)
            dat_outfile.write('\n')
            
    #Add the total tweets to the total tweets per year list
    total_tweets_list[tag_count] += total_tweets
            
    tag_count+=1

dat_outfile.close() #Close the tweets file+=1

dat_outfile.close() #Close the tweets file

# documents = line in metadata.dat

# read documents line by line and create vector
# document represented by 1 vector where ith element in vector is frequency w/ which ith word appears in document
# dictionary to map words to ids

dictionary = corpora.Dictionary(line[1].lower().split() for line in tweets)

# remove stop words and words that appear only once
stop_ids = [dictionary.token2id[stopword] for stopword in stoplist
            if stopword in dictionary.token2id]
once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.items() if docfreq == 1]
dictionary.filter_tokens(stop_ids + once_ids) # remove stop words and words that appear only once
dictionary.compactify() # remove gaps in id sequence after words that were removed

dictionary.save(os.path.join(cwd, 'analysis_static_tags','dictionary.dict')) # store the dictionary, for future reference

#Write seq file
seq_outfile = open(os.path.join(cwd, 'analysis_static_tags', 'acps-static-seq.dat'), 'w')
seq_outfile.write(str(len(total_tweets_list)) + '\n') #number of TimeStamps

for count in total_tweets_list:
    seq_outfile.write(str(count) + '\n') #write the total tweets per year (timestamp)
    
seq_outfile.close()

print('Done collecting tweets and writing seq')

#Save vocabulary
vocFile = open(os.path.join(cwd, 'analysis_static_tags', 'vocabulary.dat'),'w')
for word in dictionary.values():
    word = str(word.encode('utf-8'))
    vocFile.write(word+'\n')
    
vocFile.close()

#Prevent storing the words of each document in the RAM
class MyCorpus(object):
     def __iter__(self):
         for line in tweets:
             data = line[0]
             # assume there's one document per line, tokens separated by whitespace
             yield dictionary.doc2bow(data.lower().split())


corpus = MyCorpus()

print(corpus)

multFile = open(os.path.join(cwd, 'analysis_static_tags', 'acps-static--mult.dat'),'w')

for vector in corpus: # load one vector into memory at a time
    multFile.write(str(len(vector)) + ' ')
    for (wordID, weigth) in vector:
        multFile.write(str(wordID) + ':' + str(weigth) + ' ')

    multFile.write('\n')
    
multFile.close()

############################################################
# RUN STATIC TOPIC MODEL 
############################################################




# serialize corpus for future usage
corpora.MmCorpus.serialize(os.path.join(cwd, 'analysis_static_tags', 'corpus_static.mm'), corpus)

# loads the corpus
corpus = corpora.MmCorpus(os.path.join(cwd, 'analysis_static_tags', 'corpus_static.mm'))
print(corpus)           

dictionary = gensim.corpora.Dictionary.load(os.path.join(cwd, "analysis_static_tags","dictionary.dict"))
 
tfidf = models.TfidfModel(corpus) # step 1 -- initialize a model

# determine number of topics using hdp-lda

m_hdp = models.HdpModel(corpus, id2word=dictionary) # helps to narrow down how many topics should be chosen
m_hdp.save('m_hdp')

m_hdp.optimal_ordering()


# run hdp_to_lda
lda1 = m_hdp.hdp_to_lda() # only returns alpha and beta values
lda1b = m_hdp.suggested_lda_model()
print(lda1b)
lda1b.show_topics(20)
data = pyLDAvis.gensim.prepare(lda1b, corpus, dictionary)
pyLDAvis.save_html(data, "acps_static_vis_lda1.html")

# LSI = dimension reduction
#m_lsi = models.LsiModel(tfidf, id2word=dictionary, num_topics=150)

from time import time
import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO,
                   filename='running.log',filemode='w')


m_lda5 = models.LdaModel(corpus, id2word=dictionary, num_topics=5, update_every=1, chunksize=100, passes = 50)
m_lda5.save('topic.model5')
m_lda10 = models.LdaModel(corpus, id2word=dictionary, num_topics=10, update_every=1, chunksize=100, passes = 50)
m_lda10.save('topic.model10')
m_lda15 = models.LdaModel(corpus, id2word=dictionary, num_topics=15, update_every=1, chunksize=100, passes = 50)
m_lda15.save('topic.model15')



pyLDAvis.gensim.prepare(mlda5, corpus, dictionary, mds='mmds').save_html('acps_static_5.html')

#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_vis_data130.html")

data = pyLDAvis.gensim.prepare(m_lda140, corpus, dictionary, mds='mmds')
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_vis_dat140a.html")

data = pyLDAvis.gensim.prepare(m_lda150, corpus, dictionary, mds='mmds')
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_vis_data150.html")

data = pyLDAvis.gensim.prepare(m_lda160, corpus, dictionary, mds='mmds')
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_vis_data160.html")

data = pyLDAvis.gensim.prepare(m_lda170, corpus, dictionary, mds='mmds')
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_vis_data170.html")

data = pyLDAvis.gensim.prepare(m_lda20, corpus, dictionary, mds='mmds')
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_vis_data20.html")

# unsure the print output is corrct. seems to not update once in a while


        
        
data = pyLDAvis.gensim.prepare(lda1, corpus, dictionary, mds='mmds')
pyLDAvis.save_html(data, "acps_static_vis_data_lda1.html")


############################################################

############################################################

# TODO: Dynamic LDA

# WHEN PREPROCESSING REMEMBER: POOL TWEETS BY HASHTAG PER YEAR

# load script dyn_lda_tweets.py

# run via command line the program.
# what was the command ?????



############################################################

# CREATE BIPARTITE NETWORK FOR R SCRIPT
# script snipplet availabel in acps.py
##############################

# htag - user network (2mode)

import networkx as nx
from networkx.algorithms import bipartite

# create a dictionary with users and tags they used
dat = {'user':twp.loc[:,'from_user'], 'tag':twp.loc[:,'tags']}
user_tag = pd.DataFrame(dat)
user_tag = user_tag.to_dict("records")

# to add edges a tuple is necessary. This is a u,v 2-tuples 
user_tag_tuple = list(zip(twp.loc[:,'from_user'], twp.loc[:,'tags']))

B = nx.Graph()
B.add_nodes_from(set(twp.loc[:,'from_user']), bipartite = 'user')
B.add_nodes_from(set(twp.loc[:,'tags']), bipartite = 'tags')
B.add_edges_from(user_tag_tuple)
B.size()

nx.write_gml(B, "user_tag_graph.gml")
# topic - user network (2mode)


##############################
# TODO: CALCULATED DIVERSITY OF INDEX
# or do this in R?
##############################

