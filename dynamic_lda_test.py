#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 16:30:07 2017

@author: katerinadoyle
"""
# tutorial: https://markroxor.github.io/gensim/static/notebooks/ldaseqmodel.html
# setting up our imports
import os
import pandas as pd
from gensim.models import ldaseqmodel
from gensim.corpora import Dictionary, bleicorpus
import numpy
from gensim.matutils import hellinger
import csv
import re
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
from gensim import corpora, models, similarities #to create a dictionary

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r'(RT)', # Retweets to use
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r'(?:[\w_]+)', # other words
    r'(?:\S)' # anything else
]
    
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)
retweet_re = re.compile(r'(?<=RT\s@)([\w_]+)(?=[\s:,])', re.IGNORECASE|re.MULTILINE|re.UNICODE)			# Retweet regex statement
tweetat_re = re.compile(r'(?<!RT\s@)(?<=@)[\w_]+(?=[\s:,])', re.IGNORECASE|re.MULTILINE)	# Tweet-at regex statement

#this tokenizer recognizes RT and special twitter character as single tokens. 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s, lowercase=False):
    tokens = tokenize(s)
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
        #tokens = [token if retweet_re.search(token) else token.lower() for token in tokens]
    return tokens




# create gensim corpus, id2word and a list with the number of documents in each of your time-slices

os.chdir("/Users/katerinadoyle/Dropbox/repos/acps")
import glob #to easily list the files that are needed

all_data = pd.DataFrame() #creates blank dataframe
df = pd.read_excel("acps1.xlsx",1) #name of excle file, index for sheet, 
x = list(df) #to check column headers
df= df.drop(df.columns[[-3]], axis=1) #axes = 1 for columns, axes = 0 for rows
all_data = all_data.append(df, ignore_index=True)

df = pd.read_excel("acps2.xlsx", 1)
all_data = all_data.append(df, ignore_index=True)
#below for loop if headers don't need to be modified
#for f in glob.glob("acps*.xlsx"): #checks for files with matching names
#    df = pd.read_excel(f, 1) #reads the file using pandas
#    all_data = all_data.append(df,ignore_index=True) #appends the file using append function (concat cut also be used)

all_data.loc[:,'text'] = all_data.loc[:,'text'].str.encode('utf-8')
 #check file
all_data.describe()
all_data[:2] #look at first 5 rows
#print (list(all_data)) #get column headings


# In[6]: CREATE SAMPLE DATA

#df_small = df.sample(100, replace=False)
df_small = all_data.sample(frac=0.20)
df_small = df_small.iloc[:,[0,2,4] ]
df_small = df_small.values.tolist()

#define time slices
#time_stamp_raw = pd.to_datetime(df_small.loc[:,"time"])
#time_stamp = time_stamp_raw.dt.year
#        time_stamp.unique()

time_stamp = ['2016', '2015', '2014', '2013']

hastag='acps' #change 

cwd = os.getcwd()
dat_outfile = open(os.path.join(cwd, 'analysis', 'metadata.dat'), 'w')
dat_outfile.write('id\tdate\tcontent\n') #write header              
                 
tweets = list()
#Set total_tweets list per year, starting at 0
total_tweets_list = [0 for year in time_stamp]

#Analyze each year..

time_stamps_count = 0

for year in time_stamp: #For each year

    print('Analyzing year ' + str(year))
    
    #Set total_tweets to 0
    total_tweets = 0
    
    #For each result (tweet), get content and save it to the output file if it's not an empty line
    for line in df_small:
        
        #line = line.tolist()
        
        #utf-8 encoding
        try:
            text = line[1].decode('utf-8')
        except:
            pass

        #Remove @xxxx and #xxxxx
        #content = preprocess(line) #still includes url, @ and re. Only removes emicons
        #content = [x[0] for x in content]
        content = [word.lower() for word in text.split() if word.find('@') == -1 and word.find('#') == -1 and word.find('http') == -1]
        #content = [word.lower() for word in line[1].split() if word.find('@') == -1 and word.find('#') == -1 and word.find('http') == -1]
        
        
        #join words list to one string
        content = ' '.join(content)
        
        #remove symbols
        content = re.sub(r'[^\w]', ' ', content)
        
        #remove stop words
        content = [word for word in content.split() if word not in stopwords.words('english') and len(word) > 3 and not any(c.isdigit() for c in word)]
        
        #join words list to one string
        content = ' '.join(content)

        #Stemming and lemmatization
        lmtzr = WordNetLemmatizer()
        
        content = lmtzr.lemmatize(content)
        
        #Filter only nouns and adjectives
        tokenized = nltk.word_tokenize(content)
        classified = nltk.pos_tag(tokenized)

        content = [word for (word, clas) in classified if clas == 'NN' or clas == 'NNS' or clas == 'NNP' or clas == 'NNPS' or clas == 'JJ' or clas == 'JJR' or clas == 'JJS']
        #join words list to one string
        content = ' '.join(content)
        
        
        if len(content) > 0:
            tweets.append([line[0], content, line[2]])
            total_tweets += 1
            #right encoding
            content = content.encode('ascii', 'ignore').decode('ascii')
            #line[0] = line[0].encode('ascii', 'ignore').decode('ascii')
            #line[2] = line[2].encode('ascii', 'ignore').decode('ascii')
            dat_outfile.write(str(line[0]) + '\t' + str(line[2]) + '\t' + content)
            dat_outfile.write('\n')
            
    #Add the total tweets to the total tweets per year list
    total_tweets_list[time_stamps_count] += total_tweets
            
    time_stamps_count+=1

dat_outfile.close() #Close the tweets file

#Write seq file
seq_outfile = open(os.path.join(cwd, 'analysis', 'acps-seq.dat'), 'w')
seq_outfile.write(str(len(total_tweets_list)) + '\n') #number of TimeStamps

for count in total_tweets_list:
    seq_outfile.write(str(count) + '\n') #write the total tweets per year (timestamp)
    
seq_outfile.close()

print('Done collecting tweets and writing seq')
