#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 17:14:27 2017

@author: katerinadoyle
"""

#Set Data Directory
import os
os.chdir("/Users/katerinadoyle/Documents/gitrepo/acps")
cwd = os.getcwd()

##############################
# MODULES
##############################

# general modules
import itertools # for flatting list of lists
import pickle #to save dtm
import csv # to create output of pooled tweets 

# working with strings
import re #for regular exptression
import string

# math modules
import pandas as pd
import numpy as np

#for drawing
import matplotlib.pyplot as plt

# for txt analysis
import gensim
from gensim import corpora, models, similarities
from gensim.models.wrappers.dtmmodel import DtmModel
from sklearn.manifold import MDS
from sklearn.feature_extraction.text import CountVectorizer
import nltk #for text analytics
from nltk.tokenize import word_tokenize
# load nltk's English stopwords as variable called 'stopwords'
stoplist = nltk.corpus.stopwords.words('english')
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string

# to visualize results of text analysis
import pyLDAvis
import pyLDAvis.gensim



##############################
# EXECUTIVE FUNCTIONS
##############################

#Tokenize the tweets

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r'(RT)', # Retweets to use
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r'(?:[\w_]+)', # other words
    r'(?:\S)' # anything else
]
    
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)
retweet_re = re.compile(r'(?<=RT\s@)([\w_]+)(?=[\s:,])', re.IGNORECASE|re.MULTILINE|re.UNICODE)			# Retweet regex statement
tweetat_re = re.compile(r'(?<!RT\s@)(?<=@)[\w_]+(?=[\s:,])', re.IGNORECASE|re.MULTILINE)	# Tweet-at regex statement

#this tokenizer recognizes RT and special twitter character as single tokens. 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s, lowercase=False):
    tokens = tokenize(s)
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
        #tokens = [token if retweet_re.search(token) else token.lower() for token in tokens]
    return tokens


def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems


def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens

def extract_hash_tags(s):
    tag = [part[1:] for part in s.split() if part.startswith('#')]
    return tag

def findhashtag(tag, tweet):
    findtag = "#"+tag
    match = re.search(findtag, tweet)
    if match:
        return True
    return False



##############################
# LOAD DATA
##############################

all_data = pd.DataFrame() #creates blank dataframe
df = pd.read_excel("acps1.xlsx",1) #name of excle file, index for sheet, 
df= df.drop(df.columns[[-3]], axis=1) #axes = 1 for columns, axes = 0 for rows
all_data = all_data.append(df, ignore_index=True)

df = pd.read_excel("acps2.xlsx", 1)
all_data = all_data.append(df, ignore_index=True)
 #below for loop if headers don't need to be modified
 #for f in glob.glob("acps*.xlsx"): #checks for files with matching names
 #    df = pd.read_excel(f, 1) #reads the file using pandas
 #    all_data = all_data.append(df,ignore_index=True) #appends the file using append function (concat cut also be used)


#check file
all_data.describe()
all_data[:2] #look at first 2 rows
 #print (list(all_data)) #get column headings

#clean file: Delete empty rows, rows with NaN in column "from_user"
all_data = all_data.dropna(subset=['from_user'])


#fname = r"acps_data_tokenized.csv"
#data = pd.read_csv(fname, low_memory=False)
data = all_data
data.shape #(34312, 17)

#data.info
#data.text.describe
data.text.isnull().values.any() #check for missing value
data.columns
df = data.dropna(subset=['text'])#remove missing values
print(data.shapue[0] - df.shape[0], "missing/empty text values") #(34312, 50) (34388, 50)


##############################
# SUBSET BY USING ONLY LONG TWEETS
##############################

df['text'].describe

def count_letters(word):
    return len(word) - word.count(' ')

leng_tweets =[]
for text in df['text']:
    numb_char = count_letters(text)
    leng_tweets.append(numb_char)
    
leng_tweets = pd.DataFrame(leng_tweets)
char_mean = leng_tweets.mean() #105.89
char_median =leng_tweets.median() #115
df['lng_tweets'] = leng_tweets

df_small = df
print(df_small.shape)
#type(df_small['lng_tweets'])
df_small = df_small[df_small['lng_tweets'] > 115] 
#df_small = df_small[0:500] # run this line only to create a tiny dataframe for testing the script
print(df_small.shape)


##############################
# DESCRIPTIVES
##############################

unique_users = list(set(data.loc[:,'from_user']))
data.loc[:, 'in_reply_to_screen_name'].apply
count_replies = pd.value_counts(data['in_reply_to_screen_name'].values, sort=True)
sum(count_replies)
count_retweet = data['text'].str.count('RT')
sum(count_retweet)
count_mention = data['text'].str.count('@')
sum(count_mention) #total @ mention also if several per tweet

##############################
# GROUP TWEETS BY tags. KEEP TIME VARIABLE INTACT
##############################


df_small.columns # get the column names

# get list of unique hashtags
    
hashtag = df_small['text'].apply(extract_hash_tags).values.tolist()
htag = list(itertools.chain.from_iterable(hashtag))
htag = list(set(htag)) # return unique hashtags
htag = [re.sub(r'[^\w]','', tag) for tag in htag ] # remove special characters

# pool tweets per hashtag
             
tweet_pooled = [ ]
for tag in htag:
    tag_tweet = []
   # tag_name = tag
    for i in range(1,len(df_small)):
        if findhashtag(tag, df_small.iloc[i,2]):
            tag_tweet = [tag, df_small.iloc[i,1:18]]
            tweet_pooled.append(tag_tweet)
            
twp = pd.DataFrame.from_items(tweet_pooled).transpose()
twp['tags'] = twp.index.values
twp.index = range(0, len(twp))
twp.to_csv("tweet_pooled.csv", sep=';', mode = 'a', header = True, index=True)

twp.to_pickle("twp_static.pkl")

#twp.loc[:,'from_user'] <- 
twp_tags = twp.groupby('tags', sort=False)['text'].apply(' '.join).to_frame() # groups all columns in one row
#twp_user = twp.groupby('tags', sort=False)['from_user'].apply(' '.join).to_frame() # groups all columns in one row
twp_user = twp.groupby('tags', sort=False)['from_user'].apply(list).to_frame()

# turn index values into column 'tags'  and create numberical index
twp_tags['tags'] = twp_tags.index.values
twp_tags.index = range(0, len(twp_tags))

#twp_user = twp.groupby('from_user')['text'].apply(' '.join).to_frame()
#twp_user['user'] = twp_user.index.values
#twp_user.index = range(0, len(twp_user))

twp_pool = pd.concat([twp_tags, twp_user], axis=1) #done correctly?


twp_tags.to_pickle("twp_tags_static.pkl")
twp_user.to_pickle("twp_user_static.pkl")
twp_pool.to_pickle("twp_pool_static.pkl")

# remove hashtags from text
# somethines hashtags are used within the sentence and not as a tag at the end. 
# DECISION: keep them in

# clean up hashtags making/makers/maker. Need to do it, before pooling tweets.          
############################################################

#  PREPROCESSING FOR STATIC TOPIC MODELING

# NOTE: copied everything from dynamic lda tutorial except the creation of a sequential file
############################################################

# creates a file 'meta data' w/ info: tag + cleaned tweet(s)

dat_outfile = open(os.path.join(cwd, 'analysis_static_tags', 'metadata.dat'), 'w')

# CHANGE HEADER IF NECESSARY
dat_outfile.write('tag\tcontent\n') #write header              
                 
tweets = list()
tweets_user = list()
#Set total_tweets list per year, starting at 0
total_tweets_list = [0 for tag in htag]

#Analyze each year..

tag_count = 0

for tag in htag: #For each year

    print('Analyzing tag ' + str(tag))
    
    #Set total_tweets to 0
    total_tweets = 0
    
    #For each result (tweet), get content and save it to the output file if it's not an empty line
    for row in twp_pool.itertuples():
        
        text = row[1]
        #line = line.tolist()
        
        #utf-8 encoding
#        try:
#            text = line.decode('utf-8')
#        except:
#            pass

        #Remove @xxxx and #xxxxx
        #content = preprocess(line) #still includes url, @ and re. Only removes emicons
        #content = [x[0] for x in content]
        content = [word.lower() for word in text.split() if word.find('@') == -1 and word.find('#') == -1 and word.find('http') == -1]
        #content = [word.lower() for word in line[1].split() if word.find('@') == -1 and word.find('#') == -1 and word.find('http') == -1]
        
        
        #join words list to one string
        content = ' '.join(content)
        
        
        #remove symbols
        content = re.sub(r'[^\w]', ' ', content)
        
        #remove stop words
        content = [word for word in content.split() if word not in stopwords.words('english') and len(word) > 3 and not any(c.isdigit() for c in word)]
        
        #join words list to one string
        content = ' '.join(content)

        #Stemming and lemmatization
        lmtzr = WordNetLemmatizer()
        
        content = lmtzr.lemmatize(content)
        
        #Filter only nouns and adjectives
        tokenized = nltk.word_tokenize(content)
        classified = nltk.pos_tag(tokenized)

        content = [word for (word, clas) in classified if clas == 'NN' or clas == 'NNS' or clas == 'NNP' or clas == 'NNPS' or clas == 'JJ' or clas == 'JJR' or clas == 'JJS']
        #join words list to one string
        content = ' '.join(content)
        
        
        if len(content) > 0:
            tweets.append([content, row[0]]) # needs to be content, tag
            tweets_user.append([content, row[0], row[2]]) #content tag, user
            total_tweets += 1
            #right encoding
            content = content.encode('ascii', 'ignore').decode('ascii')
            dat_outfile.write(str(row[2]) + '\t' + content)
            dat_outfile.write('\n')
            
    #Add the total tweets to the total tweets per year list
    total_tweets_list[tag_count] += total_tweets
            
    tag_count+=1

dat_outfile.close() #Close the tweets file+=1

dat_outfile.close() #Close the tweets file

# documents = line in metadata.dat

# read documents line by line and create vector
# document represented by 1 vector where ith element in vector is frequency w/ which ith word appears in document
# dictionary to map words to ids

dictionary = corpora.Dictionary(line[1].lower().split() for line in tweets)

# remove stop words and words that appear only once
stop_ids = [dictionary.token2id[stopword] for stopword in stoplist
            if stopword in dictionary.token2id]
once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.items() if docfreq == 1]
dictionary.filter_tokens(stop_ids + once_ids) # remove stop words and words that appear only once
dictionary.compactify() # remove gaps in id sequence after words that were removed

dictionary.save(os.path.join(cwd, 'analysis_static_tags','dictionary.dict')) # store the dictionary, for future reference

#Write seq file
seq_outfile = open(os.path.join(cwd, 'analysis_static_tags', 'acps-static-seq.dat'), 'w')
seq_outfile.write(str(len(total_tweets_list)) + '\n') #number of TimeStamps

for count in total_tweets_list:
    seq_outfile.write(str(count) + '\n') #write the total tweets per year (timestamp)
    
seq_outfile.close()

print('Done collecting tweets and writing seq')

#Save vocabulary
vocFile = open(os.path.join(cwd, 'analysis_static_tags', 'vocabulary.dat'),'w')
for word in dictionary.values():
    word = str(word.encode('utf-8'))
    vocFile.write(word+'\n')
    
vocFile.close()

#Prevent storing the words of each document in the RAM
class MyCorpus(object):
     def __iter__(self):
         for line in tweets:
             data = line[0]
             # assume there's one document per line, tokens separated by whitespace
             yield dictionary.doc2bow(data.lower().split())


corpus = MyCorpus()

print(corpus)

multFile = open(os.path.join(cwd, 'analysis_static_tags', 'acps-static--mult.dat'),'w')

for vector in corpus: # load one vector into memory at a time
    multFile.write(str(len(vector)) + ' ')
    for (wordID, weigth) in vector:
        multFile.write(str(wordID) + ':' + str(weigth) + ' ')

    multFile.write('\n')
    
multFile.close()

############################################################
# RUN DYNAMIC TOPIC MODEL 
############################################################


# serialize corpus for future usage
corpora.MmCorpus.serialize(os.path.join(cwd, 'analysis_static_tags', 'corpus_static.mm'), corpus)

# loads the corpus
corpus = corpora.MmCorpus(os.path.join(cwd, 'analysis_static_tags', 'corpus_static.mm'))
print(corpus)           

dictionary = gensim.corpora.Dictionary.load(os.path.join(cwd, "analysis_static_tags","dictionary.dict"))
 
tfidf = models.TfidfModel(corpus) # step 1 -- initialize a model

# determine number of topics using hdp-lda

m_hdp = models.HdpModel(corpus, id2word=dictionary) # helps to narrow down how many topics should be chosen
m_hdp.save('m_hdp')


# run hdp_to_lda
lda1 = m_hdp.hdp_to_lda() # only returns alpha and beta values
lda1b = m_hdp.suggested_lda_model()
print(lda1b)
lda1b.show_topics(20)
data = pyLDAvis.gensim.prepare(lda1b, corpus, dictionary)
pyLDAvis.save_html(data, "acps_static_vis_lda1.html")

# LSI = dimension reduction
#m_lsi = models.LsiModel(tfidf, id2word=dictionary, num_topics=150)

from time import time
import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO,
                   filename='running.log',filemode='w')


m_lda5 = models.LdaModel(corpus, id2word=dictionary, num_topics=5, update_every=1, chunksize=100, passes = 50)
m_lda5.save(os.path.join(cwd, 'models', 'topic.model5'))
m_lda5 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model5'))

m_lda10 = models.LdaModel(corpus, id2word=dictionary, num_topics=10, update_every=1, chunksize=100, passes = 50)
m_lda10.save(os.path.join(cwd, 'models','topic.model10'))
m_lda10 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model10'))

m_lda15 = models.LdaModel(corpus, id2word=dictionary, num_topics=15, update_every=1, chunksize=100, passes = 50)
m_lda15.save(os.path.join(cwd, 'models','model15'))
m_lda15 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model15'))


data = pyLDAvis.gensim.prepare(m_lda5, corpus, dictionary)
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_lda5.html")

data = pyLDAvis.gensim.prepare(m_lda10, corpus, dictionary)
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_lda10.html")

data = pyLDAvis.gensim.prepare(m_lda15, corpus, dictionary, mds='mmds')
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_lda15.html")

# 15 too many topics
# with 10 topics some are small. run analysis with 6,7,8,9 topics

m_lda6 = models.LdaModel(corpus, id2word=dictionary, num_topics=6, update_every=1, chunksize=100, passes = 50)
m_lda6.save(os.path.join(cwd, 'models', 'topic.model6'))
m_lda6 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model6'))

m_lda7 = models.LdaModel(corpus, id2word=dictionary, num_topics=7, update_every=1, chunksize=100, passes = 50)
m_lda7.save(os.path.join(cwd, 'models','topic.model7'))
m_lda7 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model7'))

m_lda8 = models.LdaModel(corpus, id2word=dictionary, num_topics=8, update_every=1, chunksize=100, passes = 50)
m_lda8.save(os.path.join(cwd, 'models','model8'))
m_lda8 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model8'))

m_lda9 = models.LdaModel(corpus, id2word=dictionary, num_topics=9, update_every=1, chunksize=100, passes = 50)
m_lda9.save(os.path.join(cwd, 'models','model9'))
m_lda9 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model9'))

data = pyLDAvis.gensim.prepare(m_lda6, corpus, dictionary)
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_lda6.html")

data = pyLDAvis.gensim.prepare(m_lda7, corpus, dictionary)
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_lda7.html")

data = pyLDAvis.gensim.prepare(m_lda8, corpus, dictionary)
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_lda8.html")

data = pyLDAvis.gensim.prepare(m_lda9, corpus, dictionary)
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_lda9.html")

m_lda4 = models.LdaModel(corpus, id2word=dictionary, num_topics=4, update_every=1, chunksize=100, passes = 50)
m_lda4.save(os.path.join(cwd, 'models','model4'))
m_lda4 = models.LdaModel.load(os.path.join(cwd, 'models', 'topic.model4'))

data = pyLDAvis.gensim.prepare(m_lda4, corpus, dictionary)
#pyLDAvis.display(data)
pyLDAvis.save_html(data, "acps_static_lda4.html")

# Decision: Take 5 topics

m_lda5.show_topics(-1)

# similarity 
index = similarities.MatrixSimilarity(lda[corpus])
index.save("simIndex.index")
#lsi = models.LsiModel(corpus, id2word=dictionary, num_topics=5)
#index = similarities.MatrixSimilarity(lsi[corpus])
#sims = index[vec_lsi]
#sims = sorted(enumerate(sims), key=lambda item: -item[1])
#print(sims)




############################################################
# create network graph w/ topics & users
# code from https://github.com/jrladd/inaugural/blob/master/src/gensim_lda.py
# changes: addresses becomes users (variable name not changed)

#Return topic distribution for all documents (create a new combined corpus using the lda model)
lda = m_lda5
num_topics = 5

# Assigns the topics to the documents in corpus
corpus_lda = lda[corpus]
# creates for each document a list with probability of being in that topic

#Create two node lists for NetworkX
# mode 1 = topics
topics = {t[0]:t[1] for t in lda.print_topics(num_topics)}  #dict comprehension

# mode 2 = users
# first get tweets,then connect tweets to user using the twp dataset as lookup variable

addresses = list(tweets_user) #used here first twp_tags which resulted in out of index error when creating edges
# transform node 1 into 1 string. need to use list format for that. tuples can't be modified
addresses = [''.join(item[0]) for item in addresses]
# convert addressses to dict
addresses_dict = {t[1]:t[0] for t in addresses}

#Create edge list for NetworkX. Created the bigraph in R. Took 30 minutes. 
# Learn to switch from python to R when python skills not up to the task 
edges = [[(addresses[idx], t[0], t[1]) for t in y] for idx,y in enumerate(corpus_lda)]
edges = sum(edges, []) #has to be a tuple. now in a list for. 
edge_tpl = tuple(edges)

with open('edgelist.csv','w', encoding='utf-8') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(['mode1','mode2', 'weight'])
    for row in edges:
        csv_out.writerow(row)


############################################################
############################################################
############################################################

# TODO: Dynamic LDA

# WHEN PREPROCESSING REMEMBER: POOL TWEETS BY HASHTAG PER YEAR

# load script dyn_lda_tweets.py

# run via command line the program.
# what was the command ?????

# modify dataset so that the tweets are pooled per month per tag

df_small['created_at'].describe
df_small = df_small.assign(year = pd.DatetimeIndex(df_small.loc[:,'created_at']).year)
df_small = df_small.assign(month = pd.DatetimeIndex(df_small.loc[:,'created_at']).month)

# descriptives: how many tweets per month and per year

ts_y = list(df_small.loc[:,'year'].value_counts()) #2016, 2015, 2014, 2017
ts_y[0], ts_y[1], ts_y[2], ts_y[3] = ts_y[2], ts_y[1], ts_y[0], ts_y[3]

#df_small.loc[:,'month'].value_counts()
ts_m = df_small.groupby(['year', 'month']).agg({'month':{'month': lambda x: len(x[x>0])}}) 

# take out 2/2017 as only 20 entries

df = df_small.assign(htag = df_small['text'].apply(extract_hash_tags).values.tolist()) 
# for every htag that is a list, duplicate the entry one per htag

#test = pd.DataFrame([(row['text'], d) for _, row in df.iterrows() for d in row['htag']])
# works but only reutnrs text variable. I want also user, year, month
twp_dyn_r = pd.DataFrame([(row[['text', 'htag', 'month','year', 'from_user', 'created_at']], d) for _, row in df.iterrows() for d in row['htag']])
# works but puts the complete row in one cell instead of copying the row
twp_dyn = twp_dyn_r[0].apply(pd.Series)
twp_dyn_tags= twp_dyn_r[1].apply(pd.Series, index=['htags'])
twp_dyn = pd.concat([twp_dyn, twp_dyn_tags], axis = 1)
   

twp_text = twp_dyn.groupby(['year', 'month', 'htags'], sort=False)['text'].apply(' '.join).to_frame() #check code frm above if corect function to join tweets
twp_user = twp_dyn.groupby(['year', 'month', 'htags'], sort=False)['from_user'].apply(' '.join).to_frame() 
twp_pool = pd.concat([twp_text, twp_user], axis=1)
twp_pool['year'] = twp_pool.index.get_level_values('year')
twp_pool['month'] = twp_pool.index.get_level_values('month')
twp_pool['year_month'] = twp_pool['year'].map(str) + '_' + twp_pool['month'].map(str)
twp_pool['tags'] = twp_pool.index.get_level_values('htags')
twp_pool.to_csv("tweet_pooled_dynamic.csv", sep="\\", header=True, index=True)

############################################################

#  PREPROCESSING FOR DYNAMIC TOPIC MODELING

# NOTE: copied everything from dynamic lda tutorial except the creation of a sequential file
############################################################


time_stamp = ['2014_11', '2014_12', 
              '2015_1', '2015_2', '2015_3', '2015_4', '2015_5', '2015_6', '2015_7', '2015_8', '2015_9', '2015_10', '2015_11', '2015_12',
              '2016_1', '2016_2', '2016_3', '2016_4', '2016_5', '2016_6', '2016_7', '2016_8', '2016_9', '2016_10', '2016_11', '2016_12',
              '2017_1', '2017_2']


# creates a file 'meta data' w/ info: tag + cleaned tweet(s)

dat_outfile = open(os.path.join(cwd, 'analysis_dynamic_tags', 'metadata.dat'), 'w')

# CHANGE HEADER IF NECESSARY
dat_outfile.write('tag\tcontent\n') #write header              
                 
tweets = list()
tweets_user = list()
tweets_uctt = list()
#Set total_tweets list per year, starting at 0
total_tweets_list = [0 for year_month in time_stamp]

#Analyze each year..

time_stamps_count = 0

for year_month in time_stamp: #For each year

    print('Analyzing year_month ' + str(year_month))
    
    #Set total_tweets to 0
    total_tweets = 0
    
    #subset the dataset to get the ones with the matching year_month
    result = twp_pool.loc[twp_pool['year_month'] == year_month]
    
    #For each result (tweet), get content and save it to the output file if it's not an empty line
    for row in result.itertuples():
        
        text = row[1]
        #line = line.tolist()
        
        #utf-8 encoding
#        try:
#            text = line.decode('utf-8')
#        except:
#            pass

        #Remove @xxxx and #xxxxx
        #content = preprocess(line) #still includes url, @ and re. Only removes emicons
        #content = [x[0] for x in content]
        content = [word.lower() for word in text.split() if word.find('@') == -1 and word.find('#') == -1 and word.find('http') == -1]
        #content = [word.lower() for word in line[1].split() if word.find('@') == -1 and word.find('#') == -1 and word.find('http') == -1]
        
        
        #join words list to one string
        content = ' '.join(content)
        
        
        #remove symbols
        content = re.sub(r'[^\w]', ' ', content)
        
        #remove stop words
        content = [word for word in content.split() if word not in stopwords.words('english') and len(word) > 3 and not any(c.isdigit() for c in word)]
        
        #join words list to one string
        content = ' '.join(content)

        #Stemming and lemmatization
        lmtzr = WordNetLemmatizer()
        
        content = lmtzr.lemmatize(content)
        
        #Filter only nouns and adjectives
        tokenized = nltk.word_tokenize(content)
        classified = nltk.pos_tag(tokenized)

        content = [word for (word, clas) in classified if clas == 'NN' or clas == 'NNS' or clas == 'NNP' or clas == 'NNPS' or clas == 'JJ' or clas == 'JJR' or clas == 'JJS']
        #join words list to one string
        content = ' '.join(content)
        
        
        if len(content) > 0:
            tweets.append([content, row[6]]) # needs to be content, tag
            tweets_user.append([content, row[6], row[2]]) #content tag, user
            tweets_uctt.append([row[2], content, row[6], row[3]])
            total_tweets += 1
            #right encoding
            content = content.encode('ascii', 'ignore').decode('ascii')
            dat_outfile.write(str(row[2]) + '\t' + content)
            dat_outfile.write('\n')
            
    #Add the total tweets to the total tweets per year list
    total_tweets_list[time_stamps_count] += total_tweets
            
    time_stamps_count+=1

dat_outfile.close() #Close the tweets file+=1

dat_outfile.close() #Close the tweets file

# documents = line in metadata.dat

# read documents line by line and create vector
# document represented by 1 vector where ith element in vector is frequency w/ which ith word appears in document
# dictionary to map words to ids

dictionary = corpora.Dictionary(line[1].lower().split() for line in tweets)

# remove stop words and words that appear only once
stop_ids = [dictionary.token2id[stopword] for stopword in stoplist
            if stopword in dictionary.token2id]
once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.items() if docfreq == 1]
dictionary.filter_tokens(stop_ids + once_ids) # remove stop words and words that appear only once
dictionary.compactify() # remove gaps in id sequence after words that were removed

dictionary.save(os.path.join(cwd, 'analysis_dynamic_tags','dictionary.dict')) # store the dictionary, for future reference

#Write seq file
seq_outfile = open(os.path.join(cwd, 'analysis_dynamic_tags', 'acps-static-seq.dat'), 'w')
seq_outfile.write(str(len(total_tweets_list)) + '\n') #number of TimeStamps

for count in total_tweets_list:
    seq_outfile.write(str(count) + '\n') #write the total tweets per year (timestamp)
    
seq_outfile.close()

print('Done collecting tweets and writing seq')

#Save vocabulary
vocFile = open(os.path.join(cwd, 'analysis_dynamic_tags', 'vocabulary.dat'),'w')
for word in dictionary.values():
    word = str(word.encode('utf-8'))
    vocFile.write(word+'\n')
    
vocFile.close()

#Prevent storing the words of each document in the RAM
class MyCorpus(object):
     def __iter__(self):
         for line in tweets:
             data = line[0]
             # assume there's one document per line, tokens separated by whitespace
             yield dictionary.doc2bow(data.lower().split())


corpus = MyCorpus()

print(corpus)

multFile = open(os.path.join(cwd, 'analysis_dynamic_tags', 'acps-static--mult.dat'),'w')

for vector in corpus: # load one vector into memory at a time
    multFile.write(str(len(vector)) + ' ')
    for (wordID, weigth) in vector:
        multFile.write(str(wordID) + ':' + str(weigth) + ' ')

    multFile.write('\n')
    
multFile.close()

############################################################
# RUN dynamic TOPIC MODEL 
############################################################

# move dtm-darwin64 into folder with files files created above
# move folder dtm-master into folder with files created above

# run program directly via terminal.
# type in:
# 

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logging.debug("test")



# path to dtm home folder
dtm_home = os.environ.get('DTM_HOME', "dtm-master")
# path to the binary. on my PC the executable file is dtm-master/bin/dtm
dtm_path = os.path.join(dtm_home, 'bin', 'dtm-darwin64')
os.chdir("/Users/katerinadoyle/Documents/gitrepo/acps/analysis_dynamic_tags")
model = DtmModel(dtm_path, corpus, total_tweets_list, num_topics=20, id2word=dictionary)
# runs but error on line 226 in dtmmodel.py

############################################################

# CREATE BIPARTITE NETWORK FOR R SCRIPT
# script snipplet availabel in acps.py
##############################

# htag - user network (2mode)

import networkx as nx
from networkx.algorithms import bipartite

# create a dictionary with users and tags they used
dat = {'user':twp.loc[:,'from_user'], 'tag':twp.loc[:,'tags']}
user_tag = pd.DataFrame(dat)
user_tag = user_tag.to_dict("records")

# to add edges a tuple is necessary. This is a u,v 2-tuples 
user_tag_tuple = list(zip(twp.loc[:,'from_user'], twp.loc[:,'tags']))

B = nx.Graph()
B.add_nodes_from(set(twp.loc[:,'from_user']), bipartite = 'user')
B.add_nodes_from(set(twp.loc[:,'tags']), bipartite = 'tags')
B.add_edges_from(user_tag_tuple)
B.size()

nx.write_gml(B, "user_tag_graph.gml")
# topic - user network (2mode)


##############################
# TODO: CALCULATED DIVERSITY OF INDEX
# or do this in R?
##############################

