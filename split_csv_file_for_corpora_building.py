#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 19 20:29:12 2017

@author: katerinadoyle
"""

# creates a file 'meta data' w/ info: tag + cleaned tweet(s)

# set the current work directory to folder with data
import os
cwd = os.getcwd()
os.chdir('/Users/katerinadoyle/Dropbox/creativity/student_data')
cwd = os.getcwd()
cwd

import csv # to import csv file

# splits the csv file into individual documents for text analysis
# each document has the name "file_[rownumber]
counter = 1
# open the file and read each row
with open('oic_ideas_score.csv','r',  encoding='utf-8' , errors='ignore', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    header=next(reader) # skip the header row
    for row in reader: # for each row, write the content of the row in a txt file and save it
        filename = "file_{}.txt".format(str(counter))
        with open(filename, 'w', newline='') as csvfile_out:
            writer = csv.writer(csvfile_out)
            writer.writerow(row)
            counter = counter + 1

import pandas as pd

df = pd.read_csv('oic_ideas_score.csv', sep=";", encoding='ISO-8859-1', engine='python')
# file saved on windows computer. encoding needed to be latin1 (also caled iso-8859-1)

cat = df.Category

cat_label=dict()
cat_label['1'] = 'food_supply'
cat_label['2'] = 'housing'
cat_label['3'] = 'energy'
cat_label['4'] = 'climate_change'
cat_label['5'] = 'education'
cat_label['6'] = 'water_supply'
cat_label['7'] = 'health_safety'
cat_label['8'] = 'several_benefits'

cat.replace(cat_label, inplace=True)
cat_df = cat.to_frame()

filename = list()
for indexvalue in cat.index.values:
    filename.append('file_{}'.format(indexvalue))

# create a file with row number and category. This is meta information to keep track of the individual files
cat_df = cat_df.assign(filename = filename) # does this put it at the end?
cat.to_csv('categories.txt', header=None, index=True)
