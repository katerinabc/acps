
# coding: utf-8

# In[ ]:

# Research Question
#1. What information are teachers from a specific school (district) sharing on twitter using a dediced hashtag? 
#(data: twitter; bag of words approach, LDA)
#2. something more explorative?


# In[ ]:

## Steps to analyse:
# check columns match up
# unified column heading
# merge excel sheets from TAGS acps
# Interaction network: create network
# Content of interaction: clean up tweets: trailing spaces what else


# # Data import 

# In[28]:

import pandas as pd
import numpy as np

import nltk #for text analytics
from nltk.tokenize import word_tokenize
# load nltk's English stopwords as variable called 'stopwords'
stopwords = nltk.corpus.stopwords.words('english')
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")

from nltk.corpus import stopwords
import string
 

import re #for regular exptression
import os

#for drawing
import matplotlib.pyplot as plt
from sklearn.manifold import MDS


# In[1]:

#Tokenize the tweets

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""
 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r'(RT)', # Retweets to use
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r'(?:[\w_]+)', # other words
    r'(?:\S)' # anything else
]
    
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)
retweet_re = re.compile(r'(?<=RT\s@)([\w_]+)(?=[\s:,])', re.IGNORECASE|re.MULTILINE|re.UNICODE)			# Retweet regex statement
tweetat_re = re.compile(r'(?<!RT\s@)(?<=@)[\w_]+(?=[\s:,])', re.IGNORECASE|re.MULTILINE)	# Tweet-at regex statement

#this tokenizer recognizes RT and special twitter character as single tokens. 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocess(s, lowercase=False):
    tokens = tokenize(s)
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
        #tokens = [token if retweet_re.search(token) else token.lower() for token in tokens]
    return tokens


def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems


def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens


# In[ ]:

# # combine the two data frames found in the working directory. Don't put any other xls files into it. It will try to comibe them

# import glob #to easily list the files that are needed

# all_data = pd.DataFrame() #creates blank dataframe
# df = pd.read_excel("acps1.xlsx",1) #name of excle file, index for sheet, 
# x = list(df) #to check column headers
# df= df.drop(df.columns[[-3]], axis=1) #axes = 1 for columns, axes = 0 for rows
# all_data = all_data.append(df, ignore_index=True)

# df = pd.read_excel("acps2.xlsx", 1)
# all_data = all_data.append(df, ignore_index=True)
# #below for loop if headers don't need to be modified
# #for f in glob.glob("acps*.xlsx"): #checks for files with matching names
# #    df = pd.read_excel(f, 1) #reads the file using pandas
# #    all_data = all_data.append(df,ignore_index=True) #appends the file using append function (concat cut also be used)


# In[ ]:

# #check file
# all_data.describe()
# all_data[:2] #look at first 5 rows
# #print (list(all_data)) #get column headings


# In[ ]:

# clean file: Delete empty rows, rows with NaN in column "from_user"
# all_data = all_data.dropna(subset=['from_user'])


# ## CREATE NETWORK

# In[ ]:

# to create the directed network: 
# REPLY newtork
# sender: 'from_user'
# receiver: 'in_reply_to_screen_name'
# MENTION network
# sender: 'from user'
# receiver: extraced from text


# In[ ]:

# #extract subset of data: date, tweets, username
# tweets = all_data.iloc[:,:3] #w/ loc it didn't work. needed to use iloc. 
# #use .loc when indexing by column names, use .iloc when indexing by integers
# #tweets[:5]
# tweets[50:60]
# #tweets.to_csv('tweets_acps.csv', encoding = 'utf-8')


# In[ ]:

# #add rows with information about text: hashtags, text mining: bag of words, semantic

# #tweets['hashtag'] = tweets.text.split('#', expand=True)
# hashtag = pd.DataFrame(x.split('#') for x in tweets.text)
# #add column names in the form of hashtag1, hashtag2 etc
# hash_names = hashtag.columns.values

# newcolnames =[]
# for i in range (0, len(hashtag.columns)):
#     colname = "hashtag" + str(i)
#     newcolnames.append(colname)

# hashtag.columns = newcolnames


# In[ ]:

# tweets = pd.concat([tweets, hashtag], axis = 1)


# In[ ]:

# # problem: text is type pd.core.frame.DataFrame, but should be text. 
# # solution: The cell content was text, but the for loop was set up to look at the column. this was a series. 
# # rewrote beginning of for loop from "for line in tweets" to "for line in tweets['text']"
# # problem: it works with a smaller set of data. maybe some strange character in a text. maybe NA?

# for line in tweets['text']:
#     #print (line)
#     #data = re.findall("RT", line)
#     try:
#         tokens = preprocess(line)
#         data_token = pd.concat([tweets, pd.DataFrame(tokens)], axis = 1)
#     except:
#         pass
#     #print(data)
#     #    tokens = preprocess(line)
# #    twtext = tweets['text'].apply(str)
# #    tokens = preprocess(twtext)
# #    topdf = pd.DataFrame(tokens)
# #    data = pd.concat([tweets, topdf], axis=1)


# In[ ]:

# token_names = data_token.columns.values

# newcolnames_token =[]
# for i in range (0, len(data_token.columns)):
#     colname = "tok" + str(i)
#     newcolnames_token.append(colname)

# data_token.columns = newcolnames_token


# In[ ]:

# data = pd.concat([tweets, hashtag, pd.DataFrame(data_token)], axis = 1)


# In[ ]:

# data.to_csv("acps_data_tokenized.csv")


# # Text analytics: Preparation

# In[2]:

# owd = os.getcwd()
# print (owd)

#Set Data Directory
os.chdir("/Users/katerinadoyle/Dropbox/repos/acps")


# In[3]:

#clean data. take out empty cells in data['text']
#summary data['text']

fname = r"acps_data_tokenized.csv"
data = pd.read_csv(fname, low_memory=False)
data.shape
#§data.info
#data.text.describe
#data.text.isnull().values.any() #check for missing value
data.columns
df = data.dropna(subset=['text'])#remove missing values
print(df.shape, data.shape) #(34312, 50) (34388, 50)


# In[4]:

import numpy as np  # a conventional alias
from sklearn.feature_extraction.text import CountVectorizer
import pickle #to save dtm


# In[5]:

#SAMPLE FILE. DATA TOO LARGE
df['text'].describe

def count_letters(word):
    return len(word) - word.count(' ')

leng_tweets =[]
for text in df['text']:
    numb_char = count_letters(text)
    leng_tweets.append(numb_char)
    
leng_tweets = pd.DataFrame(leng_tweets)
char_mean = leng_tweets.mean() #105.89
char_median =leng_tweets.median() #115
df['lng_tweets'] = leng_tweets


# In[6]:

#df_small = df.sample(100, replace=False)
df_small = df
print(df_small.shape)
#type(df_small['lng_tweets'])
df_small = df_small[df_small['lng_tweets'] > 115]
print(df_small.shape)


# In[ ]:

#for using mallet each tweet needs to be one txt document
owd = r"/Users/katerinadoyle/Dropbox/repos/acps"
os.chdir(r"/Users/katerinadoyle/Dropbox/repos/acps")

os.chdir(r"/Users/katerinadoyle/Dropbox/repos/acps/docs")

#empty the directory first?

for i, line in enumerate(df_small['text']):
    with open("tweet_{}_{}.txt".format(str(df_small.iloc[i]['from_user']),str(i+1)), "w") as txtfile:
        txtfile.write(line)

os.chdir(owd)


# ## Text analytics: Document-Term-Matrix

# In[ ]:

#tweets_list = list(df['text'])

#potential error below: need to first create list object instead of writing it all in 1 line
vectorizer = CountVectorizer(input = list(df_small['text']), analyzer="word", tokenizer = None, preprocessor = None,
                             stop_words='english', max_features = 5000)
#print (vectorizer)
dtm = vectorizer.fit_transform(list(df_small['text']))  # a sparse matrix
dtm.shape
#error: The truth value of a Series is ambiguous. Use a.empty, a.bool(), a.item(), a.any() or a.all().
#solution: I first used df['text']. this was a series. the function needed a list.
vocab = vectorizer.get_feature_names()  # a list

#saving dtm for later use.
fname = "acps_dtm.obj"
with open(fname, "wb") as f:
    pickle.dump(dtm, f)


# In[ ]:

#measure similarity between documents using cosine. from a webpage that focuses on tweets.
# it also takes the length of tweets into account
from sklearn.metrics.pairwise import cosine_similarity
dist_c = 1 - cosine_similarity(dtm)

#saving disc for later use.
#fname = "acps_dist_c.obj"
#with open(fname, "wb") as f:
#    pickle.dump(dist_c, f)


# In[ ]:

#measuring similarity as euclidean distance. from a webpage that focuses on texts
from sklearn.metrics.pairwise import euclidean_distances

dist_e = euclidean_distances(dtm)
#saving dis_e for later use.
#fname = "acps_dist_e.obj"
#with open(fname, "wb") as f:
#    pickle.dump(dist_e, f)


# ## ##visualize distance between two tweets

# In[ ]:

# mds = MDS(n_components=2, dissimilarity="precomputed", random_state=1)

# # two components as we're plotting points in a two-dimensional plane
# # "precomputed" because we provide a distance matrix
# # we will also specify `random_state` so the plot is reproducible.

# pos = mds.fit_transform(dist_e)  # shape (n_components, n_samples)

# xs, ys = pos[:, 0], pos[:, 1]

# # short versions of filenames:
# # convert 'data/austen-brontë/Austen_Emma.txt' to 'Austen_Emma'
# names = dist_e[0]


# # color-blind-friendly palette
# # for x, y, name in zip(xs, ys, names):
# #     color = 'orange' if "Austen" in name else 'skyblue'
# #     plt.scatter(x, y, c=color)
# #     plt.text(x, y, name)

# plt.show()


# ### Clustering the data

# In[ ]:

from scipy.cluster.hierarchy import ward, dendrogram

linkage_matrix = ward(dist_e)
# match dendrogram to that returned by R's hclust()

names = dist_e[0]

dendogram_disc_e = dendrogram(linkage_matrix, orientation="right", labels=names)

#plt.tight_layout()  # fixes margins
#plt.show()
plt.savefig('acps_clustering.png', dpi=200)


# # Research question: Organizational learning = distribution of knowledge (spread & diversity)

# In[ ]:

#1: attach topic to each tweet --> most tweet content differenty (see Watson paper. most things aren't viral)
#2: measure diffusion of topic (spread)
#3: measure number of clusters as indicaton of diversity (network level)
#4: measure individual propensity for depth/breath of kw --> rwg on individual level about topics they discuss 
#5: step 4 could lend itself to test for homophily ?


# # LDA to define topics

# In[ ]:

from gensim import corpora, models, similarities 


# In[ ]:

# #tokenize
# tokenized_text = [preprocess(text) for text in df_small['text']]  
# #print(tokenized_text)

# #remove stop words
# #include these next two lines to take care of twitter specific tokens
# punctuation = list(string.punctuation)
# stop = stopwords.words('english') + punctuation + ['rt','RT','via']

# texts = [[word for word in text if word not in stop] for text in tokenized_text]
# #print(texts)


# In[ ]:

#let's just skip this for now as it is not included in this tutorial: 
#https://github.com/jmausolf/Python_Tutorials/blob/master/Topic_Models_for_Text/topic_modelr.py

# #create a Gensim dictionary from the texts
# dictionary = corpora.Dictionary(texts)

# #remove extremes (similar to the min/max df step used when creating the tf-idf matrix)
# dictionary.filter_extremes(no_below=1, no_above=0.8)

# #convert the dictionary to a bag of words corpus for reference
# corpus = [dictionary.doc2bow(text) for text in texts]


# In[ ]:

#print (tokenized_text)


# In[ ]:

# topic_clf = "lda"
# num_topics = 15
# num_top_words = 10
# ngram_lengths = [1,4]


# vectorizer = text.TfidfVectorizer(input=df_small['text'], analyzer='word', ngram_range=(ngram_lengths), stop_words='english', min_df=2)

# clf = decomposition.LatentDirichletAllocation(n_topics=num_topics+1, random_state=3)

# dtm_lda = vectorizer.fit_transform(df_small['text']).toarray()
# vocab = np.array(vectorizer.get_feature_names())


# In[ ]:

# #Fit Topic Model
# doctopic = clf.fit_transform(dtm_lda)
# topic_words = []
# for topic in clf.components_:
#     word_idx = np.argsort(topic)[::-1][0:num_top_words]
#     topic_words.append([vocab[i] for i in word_idx])


# In[ ]:

# # Show the Top Topics
# def start_csv(file_name):
#     with open(file_name, 'w') as f:
#         writer = csv.writer(f)


# In[ ]:

# os.chdir(r"/Users/katerinadoyle/Dropbox/repos/acps")


# In[ ]:

# if not os.path.exists("results"):
#  	os.makedirs("results")
# os.chdir("results")
# results_file = "clf_results_{}_model.csv".format(topic_clf)
# start_csv(results_file)
# print("writing topic model results in {}...".format("/results/"+results_file))
# for t in range(len(topic_words)):
#     print("Topic {}: {}".format(t, ', '.join(topic_words[t][:])))
#     with open(results_file, 'a') as f:
#         writer = csv.writer(f)
#         writer.writerow(["Topic {}, {}".format(t, ', '.join(topic_words[t][:]))])


# In[ ]:

#assign topic to tweet


# In[ ]:

#Let's try to use mallet as the tutorial provides instruction for assigning topics to documents


# In[ ]:

#run the LDA using mallet


# In[1]:

#combine topics and docs

import numpy as np
import itertools
import operator
import os

def grouper(n, iterable, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)


# In[6]:

os.getcwd()


# In[102]:

doctopic_triples = []

mallet_docnames = []

with open("/Users/katerinadoyle/Documents/mallet-2.0.7/results/lgt-tweet_above_median/doc-lgt-tweet_above_median.txt") as f:
    f.readline()  # read one line in order to skip the header
    f.readline() #skip another line for that .DSstore entry 
    for line in f:
        docnum, docname, *values = line.rstrip().split('\t')
        mallet_docnames.append(docname)
        for topic, share in grouper(2, values):
            triple = (docname, int(topic), float(share))
            doctopic_triples.append(triple)


# In[103]:

# sort the triples
# triple is (docname, topicnum, share) so sort(key=operator.itemgetter(0,1))
# sorts on (docname, topicnum) which is what we want
doctopic_triples = sorted(doctopic_triples, key=operator.itemgetter(0,1))
#print(doctopic_triples)

# sort the document names rather than relying on MALLET's ordering
mallet_docnames = sorted(mallet_docnames)

# collect into a document-term matrix
num_docs = len(mallet_docnames)
num_topics = len(doctopic_triples) // len(mallet_docnames)

# the following works because we know that the triples are in sequential order
doctopic = np.zeros((num_docs, num_topics))

for triple in doctopic_triples:
    docname, topic, share = triple
    #print(docname, topic, share)
    row_num = mallet_docnames.index(docname)
    doctopic[row_num, topic] = share


# In[104]:

#Now we will calculate the average of the topic shares associated with each tweet. 

CORPUS_PATH = os.path.join('/Users/katerinadoyle/Dropbox/repos/acps/docs/')
filenames = sorted([os.path.join(CORPUS_PATH, fn) for fn in os.listdir(CORPUS_PATH)])
#print(filenames)
filenames = [names for names in filenames if 'tweet' in names]


# In[105]:

tweet_names=[] #create in same format than tweet_names
tweeter =[]
for fn in filenames:
    #relevant_fn = glob.glob("tweet*")
   # relevant_docs = glob.glob(".txt") #only take those files ending with .txt
    basename = os.path.basename(fn)
    name, ext = os.path.splitext(basename)
    name_2 = name.strip('tweet_')
    name_2 = name_2.rstrip("0123456789")
    tweet_names.append(name)
    tweeter.append(name_2)
tweeter = [name.rstrip("_") for name in tweeter]


# In[106]:

# turn this into an array so we can use NumPy functions
tweet_names = np.asarray(tweet_names)


# In[107]:

# print(doctopic.shape)
# print(len(tweet_names))
# doctopic[0:2]


# In[191]:

os.chdir("/Users/katerinadoyle/Dropbox/repos/acps/results/")
doctopic_pandas = pd.DataFrame(doctopic, index = tweeter)#, columns=[1:len(doctopic)])
doctopic_pandas.to_csv("doctopics_lgt-tweet_above_median.csv")
#doctopic_pandas


# In[199]:

#GROUP BY USER
doctopic_orig = doctopic.copy()

# use method described in preprocessing section
num_groups = len(set(tweeter)) #set only records unique elements

doctopic_grouped = np.zeros((num_groups, num_topics))


# In[198]:

print (num_groups, num_btopics)


# In[228]:

name = "1Kharisma"
doctopic_pandas['tweeter'] = tweeterb


# In[232]:

#grouped mean of proportions

doctopic_grouped = doctopic_pandas.groupby(doctopic_pandas['tweeter']).mean()


# In[235]:

doctopic_grouped.shape


# In[ ]:

#source code for using LDA for tweets in python
#https://github.com/jmausolf/Python_Tutorials/blob/master/Topic_Models_for_Text/topic_modelr.py


# # Questions

# In[ ]:

#create netwrk from this edgelist --> bipartite network
#diffusion of topics
#spread of topics


# ## Create bipartite network

# In[237]:

doctopic_grp_mean_topic = doctopic_grouped.mean(axis = 0)
doctopic_grp_mean_tweet = doctopic_grouped.mean(axis = 1)


# In[242]:

len(set(tweeter))


# In[243]:

#pick the highest value
max_topic = doctopic_grouped.idxmax(axis = 1)
#doctopic = pd.DataFrame(doctopic, index = tweeter)

dat = {'user':set(tweeter), 'max_topic':max_topic}
user_topic = pd.DataFrame(dat)


# In[244]:

user_topic_dict = user_topic.to_dict("records")
user_topic_tuple = list(zip(tweeter, max_topic))
#user_topic_dict
#user_topic_list = [tweeter, max_topic]


# In[245]:

#https://networkx.github.io/documentation/networkx-1.10/reference/algorithms.bipartite.html

import networkx as nx
from networkx.algorithms import bipartite

B = nx.Graph()
B.add_nodes_from(tweeter, bipartite = 'user')
B.add_nodes_from(list(range(0,10)), bipartite = 'topic')
B.add_edges_from(user_topic_tuple)
B.size()


# In[256]:

fh = open("acps_bipartite.csv", "wb")
nx.write_edgelist(B, fh)


# In[253]:

# Variety of topics
print(round(bipartite.robins_alexander_clustering(B), 3))
#print(nx.degree_centrality(B, "user"))
#print(nx.degree_centrality(B, "topic"))


# In[247]:

#one mode projections of user-user. User's are linked if they talk about the same topic

G = bipartite.weighted_projected_graph(B, user)


# In[249]:

print(G.size())


# In[182]:

def trim_degrees(g, degree=1):
    """
    Trim the graph by removing nodes with degree less then value of the degree parameter
    Returns a copy of the graph, so it's non-destructive.
    """
    g2=g.copy()
    d=net.degree(g2)
    for n in g2.nodes():
        if d[n]<=degree: g2.remove_node(n)
    return g2


# In[ ]:




# In[ ]:

# Compile regex statements to find the date of message ("tweettime"), any retweets in message ("retweet"), any tweet-ats ("tweetat"), and the username of message author ("usr")
# from: rlsheper Twitter-SNA github

import sys
import re
from datetime import datetime, timedelta

#re.compile is done to save the pattern
tweettime = re.compile(r'(?<="[a-zA-Z]{3,3},\s)\d{2,2}\s*[a-zA-Z]{3,3}\s\d{4,4}\s(\d{2,2}:){2,2}\d{2,2}\s(\+\d{4,4})(?=")', re.MULTILINE) # Timestamp (pubDate) regex statement
retweet = re.compile(r'(?<=RT\s@)([\w_]+)(?=[\s:,])', re.IGNORECASE|re.MULTILINE|re.UNICODE)			# Retweet regex statement
tweetat = re.compile(r'(?<!RT\s@)(?<=@)[\w_]+(?=[\s:,])', re.IGNORECASE|re.MULTILINE)	# Tweet-at regex statement
#usr = re.compile(r'(?<=,")[\w\W][^,]+(?=@twitter.com \())', re.IGNORECASE|re.MULTILINE) # Username of author

#check column text for RT or mention sign. If yes, add column about receiver
#the error was that matching for word [\w_] wasn't included in its own group. What ended up working was:
#tweets['text'].str.extract(retweet, expand = False)


# In[ ]:

tweets.iloc[1:10,:3]
type(tweets.iloc[:,:3])


# In[ ]:

import networkx as nx

#network with users as nodes. hashtags as attributes

#type (tweets)
test = tweets[:50]
#print (test) #TEST FILE
#type (test)

G=nx.Graph()
nodelist = []

#index = 0
for row in tweets.itertuples():
    #print (index, row)
    #print (row)
    RT = retweet.search(row[3]) #Typeerror when NaN
    #print (RT()
    AT = tweetat.findall(row[3])    	 # Look for tweet-ats
    #print (AT) #that returns a list
    SN = row[2]           # Look for the username of the author	
    #print (SN) #that is ok without .group. 
  

  # If message is a retweet, create an edge leading from the original poster (mentioned in retweet) and the current author.
    if RT:
        #toPrint = str(RT.group() + ',' + SN + ',' + '\n')	# Create a string specifing a Retweet edge.
        #toPrintStr = toPrint.lower() 		# Set all usernames to lowercase, as twitter is case-insensative.
        G.add_edge(str(SN), str(RT.group()), 
                   type='RT', color='r', 
                   tweet=row[3],
                  hashtag = [i  for i in row[3].split() if i.startswith("#") ])
        #RT is ok.
    if AT:
        for s in AT:
            #toPrint = str(SN + ',' + s + ',' +'\n')   # Create a string specifing a Tweet-at edge.
            #toPrintStr = toPrint.lower()        # Set all usernames to lowercase.
            G.add_edge(SN, s, type='AT', color= 'b', tweet = row[3],
            hashtag = [i  for i in row[3].split() if i.startswith("#") ])
#    index +=1

G.graph       
print (G.number_of_nodes())
print (G.number_of_edges())
#print (G.edges(data=True))

#saving dtm for later use.
fname = "graph_acps.obj"
with open(fname, "wb") as f:
    pickle.dump(G, f)


# In[ ]:

#For testing: drawing the graph.

import matplotlib.pyplot as plt

#to define colors of edges
#edges = G.edges()
#colors = [G[u][v]['color'] for u,v in edges]

#nx.draw(G, edge_color=colors) #change color of edge by type. add label
#only include labels if centrality above XX?
#plt.show()


# In[ ]:

#network analysis
#cluster analysis by person, then by hashtags or knowledge being shared --> How diverse is your Twitter capital?


# In[ ]:

#take subset of data
e=G.nodes() #stores all the nodes in e which takes the form of a list
#e[1:100] #returns the 1st 100 nodes
Gsmall = G.subgraph(e[1:200])

#CLUSTERING BY HASHTAG
#use snap.py, the python interface for standford's SNAP program

#CLUSTERING BY PERSON
clst_1 = list(nx.k_clique_communities(Gsmall, 2))

#saving dtm for later use.
fname = "clusterin_node_acps.obj"
with open(fname, "wb") as f:
    pickle.dump(clst_1, f)
    
#clustering using node attribute


# In[ ]:

Gsmall.size()


# In[ ]:

G.size()

